#include "ros/ros.h"
#include "std_msgs/String.h"
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <sstream>
#include <sensor_msgs/Image.h>

image_transport::Publisher image_pub_;

void imageCb(const sensor_msgs::ImageConstPtr& msg)
{
  cv_bridge::CvImagePtr cv_ptr;

  // Read image from ROS and convert to CV image
  try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
  catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

  // Detect Red in HSV image
  cv::Mat hsv_image;
  cv::cvtColor(cv_ptr->image, hsv_image, cv::COLOR_BGR2HSV);
  
  cv::Mat lower_red_hue_range, upper_red_hue_range, red_hue_range;
  cv::inRange(hsv_image, cv::Scalar(0, 100, 100), cv::Scalar(10, 255, 255), lower_red_hue_range);
  cv::inRange(hsv_image, cv::Scalar(160, 100, 100), cv::Scalar(179, 255, 255), upper_red_hue_range);
  
  cv::addWeighted(lower_red_hue_range, 1.0, upper_red_hue_range, 1.0, 0.0, red_hue_range);

  cv::cvtColor(red_hue_range, cv_ptr->image, cv::COLOR_GRAY2BGR);


  image_pub_.publish(cv_ptr->toImageMsg());

}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "fake_led_marker");

  ros::NodeHandle nh_;

  image_transport::ImageTransport it_(nh_);
  image_transport::Subscriber image_sub_ = it_.subscribe("/image_raw", 1, imageCb);
  image_pub_ = it_.advertise("/image_converter/output_video", 1);

  ros::Rate loop_rate(10);

  int count = 0;
  while (ros::ok())
    {

      ros::spinOnce();

      loop_rate.sleep();
      ++count;
    }


  return 0;
}
