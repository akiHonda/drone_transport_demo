#!/usr/bin/env python
import rospy
import smach
import smach_ros
from std_msgs.msg import String
from std_srvs.srv import Empty
import time
import threading
from ar_track_alvar_msgs.msg import AlvarMarkers
import std_msgs.msg

# define state Bar
class StartStateMachine(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success'])

    def execute(self, userdata):
        rospy.sleep(5)
        return 'success'



class PTAMInit(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success', 'abort', 'failed'])
        self.mutex = threading.Lock()
        self.stable_state = False
        self.subscriber = rospy.Subscriber('tum_ardrone/com', String, self.callback)
        self.com_pub = rospy.Publisher('tum_ardrone/com', String, queue_size=1)

        self.counter = 0

    def callback(self, data):
        self.mutex.acquire()
        
        data_str = data.data
        #print data_str
        if 'PTAM: Best' in data_str :
            self.stable_state = True
        
        self.mutex.release()

    def execute(self, userdata):
        # Only 3 times
        if self.counter >= 3:
            return 'failed'
        else:
            self.counter += 1

        # Send command
        rospy.sleep(1.0)
        self.com_pub.publish("c clearCommands")
        rospy.sleep(0.1)
        self.com_pub.publish("c autoInit 500 800 4000 0.5")
        rospy.sleep(0.1)
        self.com_pub.publish("c setReference $POSE$")
        rospy.sleep(0.1)
        self.com_pub.publish("c setInitialReachDist 0.2")
        rospy.sleep(0.1)
        self.com_pub.publish("c setStayWithinDist 0.2")
        rospy.sleep(0.1)
        self.com_pub.publish("c setStayTime 3")
        rospy.sleep(0.1)
        self.com_pub.publish("c lockScaleFP")
        rospy.sleep(0.1)
        self.com_pub.publish("c start")
        rospy.sleep(0.1)

        #wait for a maximum of 30 seconds 
        for i in range(0, 200):
            self.mutex.acquire()
            if self.stable_state:
                self.com_pub.unregister()
                return 'success'

            self.mutex.release()

            time.sleep(.1)
            #still waiting
            #return 'in_progress'
        #we didn't get 2 in the 30 sec
        return 'abort'

class LandReset(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success'])
        self.land_pub = rospy.Publisher('ardrone/land', std_msgs.msg.Empty, queue_size=1)
        self.com_pub = rospy.Publisher('tum_ardrone/com', String, queue_size=1)

    def execute(self, userdata):

        # Send command
        rospy.sleep(0.1)
        self.land_pub.publish(std_msgs.msg.Empty())

        rospy.sleep(0.1)
        self.com_pub.publish("c stop")
        rospy.sleep(0.1)
        self.com_pub.publish("c clearCommands")
        rospy.sleep(0.1)
        self.com_pub.publish("f reset")

        self.land_pub.unregister()
        self.com_pub.unregister()
        return 'success'

### Seek Marker
class SeekMarker(smach.State):
    def __init__(self):
        smach.State.__init__(self, 
                             outcomes=['success','failed'],
                             output_keys=['seek_marker_out'])
        self.subscriber = rospy.Subscriber('ar_pose_marker', AlvarMarkers, self.callback)
        self.data_counter = 0
        self.mutex = threading.Lock()

        self.x = 0
        self.y = 0
        self.z = 0

    def callback(self, data):
        self.mutex.acquire()

        if len(data.markers) > 0:
            self.x += data.markers[0].pose.pose.position.x
            self.y += data.markers[0].pose.pose.position.z
            self.z += -data.markers[0].pose.pose.position.y
            self.data_counter += 1

        self.mutex.release()


    def execute(self, userdata):
        for i in range(0, 100):
            self.mutex.acquire()
            if self.data_counter > 10:
                self.x = self.x / self.data_counter
                self.y = self.y / self.data_counter
                self.z = self.z / self.data_counter
                #print self.x
                #print self.y
                #print self.z
                userdata.seek_marker_out = [self.x, self.y, self.z]
                return 'success'

            self.mutex.release()

            time.sleep(.1)

        userdata.seek_marker_out = [0,0,0]
        return 'failed'

class LandReset(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success'])
        self.land_pub = rospy.Publisher('ardrone/land', std_msgs.msg.Empty, queue_size=1)
        self.com_pub = rospy.Publisher('tum_ardrone/com', String, queue_size=1)

    def execute(self, userdata):

        # Send command
        rospy.sleep(0.1)
        self.land_pub.publish(std_msgs.msg.Empty())

        rospy.sleep(0.1)
        self.com_pub.publish("c stop")
        rospy.sleep(0.1)
        self.com_pub.publish("c clearCommands")
        rospy.sleep(0.1)
        self.com_pub.publish("f reset")

        self.land_pub.unregister()
        self.com_pub.unregister()
        return 'success'

class Move2Marker(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success', 'failed'],
                             input_keys=['move_2_marker_in'])
        self.com_pub = rospy.Publisher('tum_ardrone/com', String, queue_size=10)
        self.reached = False


    def callback(self, data):
        self.mutex.acquire()

        data_str = data.data
        #print data_str                                                                                                                                                                                      
        if 'Queue: 0' in data_str :
            self.reached = True

        self.mutex.release()

    def execute(self, userdata):
        
        # Send command
        rospy.sleep(1.0)
        self.com_pub.publish("c clearCommands")
        #self.com_pub.publish("c setReference $POSE$")
        
        str_msg = String()
        print str_msg
        str_msg.data = "c setReference " + str(userdata.move_2_marker_in[0]) + " " + str(userdata.move_2_marker_in[1]) + " " + str(userdata.move_2_marker_in[2]) + " 0"
        self.com_pub.publish(str_msg)
                
        self.com_pub.publish("c goto 0 -1.8 0 0")
        self.com_pub.publish("c goto 0.5 -1.8 0 0")
        self.com_pub.publish("c goto -0.5 -1.8 0 0")
        rospy.sleep(0.1)
        
        
        self.com_pub.publish("c start")
        
        self.subscriber = rospy.Subscriber('tum_ardrone/com', String, self.callback)
        self.mutex = threading.Lock()
        
        for i in range(0, 500):
            self.mutex.acquire()
            if self.reached:
                self.com_pub.unregister()
                return 'success'
        
            self.mutex.release()

            time.sleep(.1)
            
        #we didn't get 2 in the 30 sec                                                                                                                                                                       
        return 'failed'

class Move2Pos(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['success', 'failed'])
                             #input_keys=['move_2_pos_in'])
        self.com_pub = rospy.Publisher('tum_ardrone/com', String, queue_size=10)
        
    def execute(self, userdata):
        
        # Send command
        rospy.sleep(0.1)
        #self.com_pub.publish("c clearCommands")
        str_msg = String()
        rospy.sleep(0.1)
        #print str_msg
        #str_msg.data = "c goto " + str(userdata.move_2_pos_in[0]) + " " + str(userdata.move_2_pos_in[1]) + " " + str(userdata.move_2_pos_in[2]) + " 0"
        str_msg.data = "c goto 2 0 0 0"
        self.com_pub.publish(str_msg)
        rospy.sleep(0.1)
        self.com_pub.publish("c start")
        #self.com_pub.unregister()
        return 'success'

if __name__ == '__main__':

    # Initial Setup
    
    # Wait Autopilot service and start
    rospy.wait_for_service('/drone_autopilot/start')
    start_autopilot = rospy.ServiceProxy('/drone_autopilot/start', Empty)

    rospy.init_node('test_publisher', anonymous=True)

    try:
        srv_resp = start_autopilot()
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e
        

    # Create a SMACH state machine
    smachine = smach.StateMachine(outcomes=['Success', 'Failed'])
    smachine.userdata.marker_position = [0,0,0]

    with smachine:
        smach.StateMachine.add('Start', StartStateMachine(), 
                               transitions={'success':'Initialize_Drone'})

        smach.StateMachine.add('Initialize_Drone', PTAMInit(), 
                               transitions={'success':'Seek_Marker', 
                                            'abort':'Initialize_Drone',
                                            'failed':'LandReset'})

        smach.StateMachine.add('Seek_Marker', SeekMarker(), 
                               transitions={'success':'Move_2_Marker',
                                            'failed':'Failed'},
                               remapping={'seek_marker_out':'marker_position'})

        smach.StateMachine.add('Move_2_Marker', Move2Marker(), 
                               transitions={'success':'Success',
                                            'failed':'Failed'},
                               remapping={'move_2_marker_in':'marker_position'})

        smach.StateMachine.add('Move_2_Position', Move2Pos(), 
                               transitions={'success':'Success',
                                            'failed':'Failed'})
                               
        smach.StateMachine.add('LandReset', LandReset(), 
                               transitions={'success':'Success'})
                                            



    # Create and start the introspection server
    sis = smach_ros.IntrospectionServer('server_name', smachine, '/SM_ROOT')
    sis.start()

    # Execute SMACH plan
    outcome = smachine.execute()

    rospy.spin()
    sis.stop()
