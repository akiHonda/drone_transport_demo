#!/usr/bin/env python
import rospy
from ar_track_alvar_msgs.msg import AlvarMarkers

def callback(data):
    
    if len(data.markers) > 0:
        x = data.markers[0].pose.pose.position.x
        y = data.markers[0].pose.pose.position.y
        z = data.markers[0].pose.pose.position.z

        rospy.loginfo("Target [%f, %f, %f]",x, z, -y)

def listener():

    rospy.init_node('marker_pose_listener', anonymous=True)

    rospy.Subscriber("ar_pose_marker", AlvarMarkers, callback)

    rospy.spin()
        
if __name__ == '__main__':
    listener()
