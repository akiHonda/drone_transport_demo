#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from std_srvs.srv import Empty

if __name__ == '__main__':

    # Initial Setup
    com_pub = rospy.Publisher('tum_ardrone/com', String, queue_size=1)
    rospy.wait_for_service('/drone_autopilot/start')
    start_autopilot = rospy.ServiceProxy('/drone_autopilot/start', Empty)


    rospy.init_node('test_publisher', anonymous=True)

    try:
        srv_resp = start_autopilot()
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e
        


    str_msg = String()
    rospy.sleep(0.1)
    str_msg = "c clearCommands"
    com_pub.publish(str_msg)
    rospy.sleep(0.1)
    com_pub.publish("c autoInit 500 800 4000 0.5")
    rospy.sleep(0.1)
    com_pub.publish("c setReference $POSE$")
    rospy.sleep(0.1)
    com_pub.publish("c setInitialReachDist 0.2")
    rospy.sleep(0.1)
    com_pub.publish("c setStayWithinDist 0.3")
    rospy.sleep(0.1)
    com_pub.publish("c setStayTime 3")
    rospy.sleep(0.1)
    com_pub.publish("c lockScaleFP")
    rospy.sleep(0.1)
    com_pub.publish("c start")

    rospy.loginfo("start")

    # spin() simply keeps python from exiting until this node is stopped
    #rospy.spin()


