#!/usr/bin/env python  
import roslib
import rospy
import math
import tf
from geometry_msgs.msg import Twist

if __name__ == '__main__':
    rospy.init_node('ar_control')

    listener = tf.TransformListener()

    vel_pub = rospy.Publisher('cmd_vel', Twist, queue_size=10)

    rate = rospy.Rate(2.0)
    while not rospy.is_shutdown():
        cmd_msg = Twist()
        kp_rot = 0.2
        kp_trans = 0

        try:
            (trans,rot) = listener.lookupTransform('/ardrone_base_bottomcam', '/ar_marker_0', rospy.Time.now() - rospy.Duration(0.5))
            (R, P, Y) = tf.transformations.euler_from_quaternion(rot)
            
            norm = math.sqrt(trans[0]*trans[0] + trans[1]*trans[1])

            cmd_msg.angular.z = -Y * 10 * kp_rot
            cmd_msg.linear.x = -( trans[0]*math.cos(Y) - trans[1]*math.sin(Y) ) * kp_trans
            cmd_msg.linear.y = -( trans[0]*math.sin(Y) + trans[1]*math.cos(Y) ) * kp_trans
            #cmd_msg.linear.x = -( trans[0] ) * kp_trans
            #cmd_msg.linear.y = -( trans[1] ) * kp_trans

            cmd_msg.linear.z =  ( 0.7 - trans[2] ) * 1.0
                        
            vel_pub.publish(cmd_msg)

            print str(trans) + ", norm=" + str(norm) + ", " +  str(Y/3.14*180) + "[deg] (" + str(cmd_msg.linear.x) + ", " + str(cmd_msg.linear.y) + ")"
            
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            print str(cmd_msg.linear.x) + ", " + str(cmd_msg.linear.y) + ")"
            vel_pub.publish(cmd_msg)
            #continue

        rate.sleep()
